/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.safaricom.controllers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import ke.co.safaricom.service.ImageService;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author CWEKESA
 */
@RestController
@RequestMapping(value="/app")
public class ImageController {
    
    @Autowired
    private ImageService menuService;
    private final String file_path=File.separator+"opt"+File.separator+"images"+File.separator+"rentals"+File.separator;
    @RequestMapping(value="/images/{id}",method = RequestMethod.GET,produces = MediaType.IMAGE_PNG_VALUE)
    public ResponseEntity<byte[]> getImage(@PathVariable int id) throws IOException{
        //get image base64
        String image=menuService.getImage(id);
        byte[] imageByte =Base64.decodeBase64(image);
        String image_name="tester"+id;
        image=image.replace("data:image/jpeg;base64,", "");//for jpeg
        image=image.replace("data:image/png;base64,", "");//for png
        imageByte =Base64.decodeBase64(image);
        File yourFile = new File(file_path);
        if (!yourFile.exists()) {
            yourFile.mkdirs();
            new File(file_path+image_name).createNewFile();
        }
        File img=new File(file_path+image_name);
        // Write a image byte array into file system
        try ( FileOutputStream imageOutFile = new FileOutputStream(file_path+image_name)) {
            imageOutFile.write(imageByte);
        }
        return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG)
                .body(Files.readAllBytes(img.toPath()));
    }
    
    
}
