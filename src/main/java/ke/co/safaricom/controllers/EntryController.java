/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.safaricom.controllers;

import java.net.InetAddress;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author CWEKESA
 */

@RestController
@RequestMapping(value="/",produces="application/json")
public class EntryController {
    
    @RequestMapping(method = RequestMethod.GET)
    public String intro() {
        return details().toString();
    }
    @Autowired
    private Environment env;
    @RequestMapping(value="/info",method = RequestMethod.GET)
    public String intro2() {
        return details().toString();
    }
    JSONObject details(){
       JSONObject response=new JSONObject();
        try{
            response.put("message", "Welcome to "+env.getProperty("spring.application.name"));
            response.put("app", env.getProperty("spring.application.name"));
            response.put("host_address", InetAddress.getLocalHost().getHostAddress());
            response.put("host_name", InetAddress.getLocalHost().getHostName());
        }
        catch(Exception e){
        }
        return response; 
    }
    
}
