/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.safaricom.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ke.co.safaricom.service.CallbackService;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author CWEKESA
 */

@RestController
@RequestMapping(value="/app/callbacks",produces="application/json")
public class CallbacksController {

    @Autowired(required=true)
    private CallbackService callbackService;
    
    @RequestMapping(method = RequestMethod.POST)
    public String getBotbydetails(@RequestBody String req) {
        return callbackService.callbackProcessor(req);
    }
}

