/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.safaricom.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ke.co.safaricom.service.TelegramService;
import org.springframework.web.bind.annotation.PathVariable;

/**
 *
 * @author CWEKESA
 */

@RestController
@RequestMapping(value="/app/telegram",produces="application/json")
public class TelegramController {

    @Autowired(required=true)
    private TelegramService telegramService;
    
    @RequestMapping(method = RequestMethod.POST)
    public String getBotbydetails(@RequestBody String req) {
        return telegramService.processInput(null,req);
    }
    @RequestMapping(value="/{id}",method = RequestMethod.POST)
    public String getBotbydetailsMultiple(@PathVariable String id,@RequestBody String req) {
        return telegramService.processInput(id,req);
    }
    
}

