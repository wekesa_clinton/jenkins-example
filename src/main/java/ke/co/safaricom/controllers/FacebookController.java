/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.safaricom.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ke.co.safaricom.service.FacebookService;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author CWEKESA
 */

@RestController
@RequestMapping(value="/app/facebook",produces="application/json")
public class FacebookController {

    @Autowired(required=true)
    private FacebookService facebookservice;
    
    @RequestMapping(method = RequestMethod.POST)
    public String getBotbydetails(@RequestBody String req) {
        return facebookservice.processInput(req);
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public String getInitializer(@RequestParam("hub.verify_token") String token,@RequestParam("hub.challenge") String chal) {
        return facebookservice.processInit(token, chal);
    }
    
}

