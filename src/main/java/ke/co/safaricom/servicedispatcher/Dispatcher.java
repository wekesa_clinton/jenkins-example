/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.safaricom.servicedispatcher;


import ke.co.safaricom.config.LogsManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;


@Service
public class Dispatcher {
    @Autowired
    LogsManager logs;
    private final Logger logger = LoggerFactory.getLogger(Dispatcher.class);
    
    private String chat_ai_endpoint="";
    
    private final String queuename="chat-ai-queue";
    private final String callback_queuename="callback_queue";
    @Autowired
    RestTemplate restTemplate;
    
    @Autowired
    private Environment env;
    
    @JmsListener(destination = queuename, containerFactory = "jmsFactory")
    public void receiveSocialMediaMessage(String message) {
        try{
            chat_ai_endpoint=env.getProperty("chat-ai-url");
            dispatcher("sendchat",message);
        }
        catch(RestClientException e){
           logger.error("error.."+logs.addlogger(500, e.getMessage(), e.getLocalizedMessage()));
        }
    }
    
    @JmsListener(destination = callback_queuename, containerFactory = "jmsFactory")
    public void receiveCallbackMessage(String message) {
        try{
            chat_ai_endpoint=env.getProperty("chat-ai-url");
            dispatcher("callbacks",message);
        }
        catch(RestClientException e){
           logger.error("error.."+logs.addlogger(500, e.getMessage(), e.getLocalizedMessage()));
        }
    }
    
    public void dispatcher(String url,String message){
        try{
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> entity = new HttpEntity<>(message, headers);
            ResponseEntity<String> quoteResponse = restTemplate.exchange(chat_ai_endpoint+url, HttpMethod.POST,entity, String.class);
            logger.info("success.."+logs.addlogger(200, "Response from Chat-ai-service", quoteResponse.getBody()));
        }
        catch(RestClientException e){
           logger.error("error.."+logs.addlogger(500, e.getMessage(), e.getLocalizedMessage()));
        }
    }
}
