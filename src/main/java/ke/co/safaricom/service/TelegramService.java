/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.safaricom.service;

import ke.co.safaricom.config.Props;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ke.co.safaricom.config.LogsManager;

@Service
public class TelegramService {
    @Autowired
    LogsManager logs;
    private final Logger logger = LoggerFactory.getLogger(TelegramService.class);
    
    private final String chat_ai_endpoint="http://chat-ai-service/sendchat";
    
    private final String verify_token="safaricom_web_test_bot";
    private final String RESPONSE_CODE="response_code";
    private final String DESCRIPTION="description";
    
    private final String queuename="chat-ai-queue";
    
    @Autowired
    RestTemplate restTemplate;
    
    @Autowired
    JmsTemplate jmsTemplate;
    
    @Autowired
    Props props;
    
    public String processInput(String bot_id,String input){
        JSONObject response=new JSONObject();
       try{
           if(input.equals("")||input==null){
               response.put(RESPONSE_CODE, "99");
               response.put(DESCRIPTION, "You sent empty input");
           }else{
               JSONObject request=new JSONObject(input);
               JSONObject messagebody=request.getJSONObject("message");
               
               String chattype=messagebody.getJSONObject("chat").getString("type");
               int senderid=messagebody.getJSONObject("from").getInt("id");
               String first="";
               String second="";
               if(messagebody.getJSONObject("from").has("first_name")){
                  first=messagebody.getJSONObject("from").getString("first_name"); 
               }
               if(messagebody.getJSONObject("from").has("last_name")){
                  second=messagebody.getJSONObject("from").getString("last_name"); 
               }
               String names=first+" "+second;
               String text="";
               if(messagebody.has("entities")){
                 text=messagebody.getString("text");
                 text=text.replace("/", "");
               }
               
               if(messagebody.has("text")){
                 text=messagebody.getString("text");
               }
               
               JSONObject queuedata=new JSONObject();
               if(text==null){
                   queuedata.put("wrong_input", true);  //we have to notify them of this,,, not supported for now
               }
               
               queuedata.put("senderid", senderid+"");
               queuedata.put("message", text);
               queuedata.put("channel", "TELEGRAM");
               queuedata.put("names", names);
               queuedata.put("channel_secret_id", bot_id);
               jmsTemplate.convertAndSend(props.CHAT_AI_QUEUE, queuedata.toString());
               logger.info(logs.addlogger(200, "Telegram data to queue", queuedata.toString()));
               response.put(RESPONSE_CODE, 0);
               response.put(DESCRIPTION, "Acknowledgement of receipt");
               logger.info(logs.addlogger(200, "Telegram response", response.toString()));
           }
       } 
       catch(JSONException e){
           logger.error("error.."+logs.addlogger(500, e.getMessage(), e.getLocalizedMessage()));
       }
      return response.toString();
    }
    
    
    
    
    
}
