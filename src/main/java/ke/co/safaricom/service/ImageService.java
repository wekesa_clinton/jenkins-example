/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.safaricom.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author CWEKESA
 */
@Service
public class ImageService {
    
    private final String chat_ai_endpoint="http://chat-ai-service/";
    
    @Autowired
    RestTemplate restTemplate;
    
    public String getMenus(){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>("", headers);
        ResponseEntity<String> quoteResponse = restTemplate.exchange(chat_ai_endpoint+"chat-manager/getmenu", HttpMethod.GET,entity, String.class);
        return quoteResponse.getBody();
       }
    
    public String addMenu(String input_menu){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(input_menu, headers);
        ResponseEntity<String> quoteResponse = restTemplate.exchange(chat_ai_endpoint+"chat-manager/addmenu", HttpMethod.POST,entity, String.class);
        return quoteResponse.getBody();
    }
    public String getImage(int id){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>("", headers);
        ResponseEntity<String> quoteResponse = restTemplate.exchange(chat_ai_endpoint+"images/getimage/"+id, HttpMethod.GET,entity, String.class);
        return quoteResponse.getBody();
    }
    
}
