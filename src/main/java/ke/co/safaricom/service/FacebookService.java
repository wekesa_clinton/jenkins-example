/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.safaricom.service;

import java.util.Date;
import ke.co.safaricom.config.Props;
import ke.co.safaricom.config.LogsManager;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class FacebookService {
    @Autowired
    LogsManager logs;
    private final Logger logger = LoggerFactory.getLogger(FacebookService.class);
   
    private final String verify_token="safaricom_web_test_bot";
    private final String RESPONSE_CODE="response_code";
    private final String DESCRIPTION="description";
    
    @Autowired
    JmsTemplate jmsTemplate;
    
    @Autowired
    Props props;
    
    public String processInit(String token,String chal){
        JSONObject response=new JSONObject();
        logger.info(logs.addlogger(200, "Processing reqistration FB", "Registeration"));
        if(token==null){
            response.put(RESPONSE_CODE, "99");
            response.put(DESCRIPTION, "Error in the request as no required parameters added");
            return response.toString();
        }
        else{
            if(token.equals(verify_token)){
                response.put(RESPONSE_CODE, "0");
                response.put(DESCRIPTION, chal.toString());
                return chal;
            }
            else{
                response.put(RESPONSE_CODE, "99");
                response.put(DESCRIPTION, "Error in the request as no match from facebook");
                return response.toString();
            }
        }
    }
    public String processInput(String input){
        JSONObject response=new JSONObject();
        try{
            if(input.equals("")||input==null){
                response.put(RESPONSE_CODE, "99");
                response.put(DESCRIPTION, "You sent empty input");
            }
            else{
                JSONObject request=new JSONObject(input);
                JSONObject queuedata=new JSONObject();
                JSONObject entry=new JSONObject(request.getJSONArray("entry").get(0).toString());
                JSONObject messaging=new JSONObject(entry.getJSONArray("messaging").get(0).toString());
                //now we get individual details
                JSONObject sender=messaging.getJSONObject("sender");
                JSONObject page_id=messaging.getJSONObject("recipient");
                String userinput="qwertyuiop";
                if(messaging.has("message")){
                    JSONObject message=messaging.getJSONObject("message");
                    if(message.has("text")){
                        userinput =message.getString("text");
                        if(message.has("quick_reply")){//now we have to take the quick reply pay_load
                            JSONObject quick_reply=message.getJSONObject("quick_reply");
                            userinput =quick_reply.getString("payload");
                        }
                    }
                    else{
                        queuedata.put("wrong_input", true);  //we have to notify them of this,,, not supported for now
                    }
                }
                if(messaging.has("postback")){
                    JSONObject message=messaging.getJSONObject("postback");
                    userinput =message.getString("payload");
                }
                queuedata.put("senderid", sender.getString("id"));
                queuedata.put("message", userinput);
                queuedata.put("channel", "FACEBOOK");
                queuedata.put("channel_secret_id", page_id.getString("id"));
                logger.info("success.."+logs.addlogger(200, "FB data to queue", queuedata.toString()));
                jmsTemplate.convertAndSend(props.CHAT_AI_QUEUE, queuedata.toString());
                response.put(RESPONSE_CODE, 0);
                response.put(DESCRIPTION, "Acknowledgement of receipt");
                logger.info("success.."+logs.addlogger(200, "Response FB", response.toString()));
            }
        }
        catch(Exception e){
            logger.error("error.."+logs.addlogger(500, e.getMessage(), e.getLocalizedMessage()));
        }
      return response.toString();
    }   
}
