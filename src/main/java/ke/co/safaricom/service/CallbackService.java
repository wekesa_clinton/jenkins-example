/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.safaricom.service;

import ke.co.safaricom.config.LogsManager;
import ke.co.safaricom.config.Props;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
/**
 *
 * @author CWEKESA
 */
@Service
public class CallbackService {
    
    @Autowired
    LogsManager logs;
    private final Logger logger = LoggerFactory.getLogger(CallbackService.class);
    
    @Autowired
    JmsTemplate jmsTemplate;
    @Autowired
    Props props;
    
    public String callbackProcessor(String input)
   { 
       JSONObject response=new JSONObject();
       try{
           response.put("RESPONSE_CODE", "99");
           response.put("DESCRIPTION", "error with the JSON");
           if(input.equals("")||input==null){
               response.put("RESPONSE_CODE", "99");
               response.put("DESCRIPTION", "You sent empty input");
           }
           else{
               //forward the request to bot-ai
               jmsTemplate.convertAndSend(props.CALLBACK_CHAT_AI_QUEUE, input);
               response.put("RESPONSE_CODE", "00");
               response.put("DESCRIPTION", "Recieved successfully");
           }
       }catch(Exception e){
           logger.error("error.."+logs.addlogger(500, e.getMessage(), e.getLocalizedMessage()));
       }
       return response.toString();
   }
    
}
