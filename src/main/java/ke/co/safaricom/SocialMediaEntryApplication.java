package ke.co.safaricom;

import javax.jms.ConnectionFactory;
import ke.co.safaricom.config.LogsManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;

@EnableJms
@SpringBootApplication
public class SocialMediaEntryApplication {

	public static void main(String[] args) {
		SpringApplication.run(SocialMediaEntryApplication.class, args);
	}
        @Bean
    public JmsListenerContainerFactory<?> jmsFactory(ConnectionFactory connectionFactory,
            DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        configurer.configure(factory, connectionFactory);
        factory.setErrorHandler(e -> {
            logger.error("Error..."+logs.addlogger(500, e.getMessage(), e.getLocalizedMessage()));
        });
        return factory;
    }
    @Autowired
    LogsManager logs;
    private final Logger logger = LoggerFactory.getLogger(SocialMediaEntryApplication.class);
}
