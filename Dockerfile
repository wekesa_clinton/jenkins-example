FROM openjdk:8-jre-alpine
ADD entry-app.jar entry-app.jar
EXPOSE 8083
ENTRYPOINT ["java","-jar","entry-app.jar"]